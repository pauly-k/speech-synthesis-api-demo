
let words = [
  { phrase: 'learn german', lang: 'en-US' },
  { phrase: 'read a book', lang: 'en-US' },
  { phrase: 'write an email', lang: 'en-US' },
  { phrase: 'message a friend', lang: 'en-US' },
  { phrase: 'หาเพื่อน', lang: 'th-TH' }
]


startup()

async function startup() {
  await ensureSpeechSysnthesisVoicesLoaded()
  populateWords()
}

function populateWords() {
  let list = document.querySelector('.list')
  for (let item of words) {
    let el = document.createElement('span')
    el.className = 'word'
    el.textContent = item.phrase
    el.onclick = wordClick.bind(el, item.phrase, item.lang )
    list.append(el)
  }
}

async function wordClick(word, lang) {
  let msg = new SpeechSynthesisUtterance(word)
  msg.rate = 1
  msg.lang = lang
  window.speechSynthesis.speak(msg)
}


function ensureSpeechSysnthesisVoicesLoaded() {
  return new Promise(
    function (resolve, reject) {
      let synth = window.speechSynthesis
      let id

      id = setInterval(() => {
        if (synth.getVoices().length !== 0) {
          resolve(synth.getVoices())
          clearInterval(id)
        }
      }, 10)
    }
  )
}